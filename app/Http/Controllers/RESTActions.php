<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

trait RESTActions {

    /**
     * Show all record
     * 
     * @return JSON
     */
    public function all()
    {
        $m = self::MODEL;
        return $this->respond(Response::HTTP_OK, $m::all());
    }

    /**
     * Show one record by ID
     * 
     * @param int $id
     * @return JSON
     */
    public function get($id)
    {
        $m = self::MODEL;
        $model = $m::find($id);
        if(is_null($model)){
            return $this->respond(Response::HTTP_NOT_FOUND);
        }
        return $this->respond(Response::HTTP_OK, $model);
    }

    /**
     * Create record
     * 
     * @param Request $request
     * @return Eloquent - new record
     */
    public function add(Request $request)
    {
        $m = self::MODEL;
        $this->validate($request, $m::$rules);
        return $this->respond(Response::HTTP_CREATED, $m::create($request->all()));
    }

    /**
     * Update record by ID
     * 
     * @param Request $request
     * @param int $id
     * @return Eloquent - updated record
     */
    public function put(Request $request, $id)
    {
        $m = self::MODEL;
        $this->validate($request, $m::$rules);
        $model = $m::find($id);
        if(is_null($model)){
            return $this->respond(Response::HTTP_NOT_FOUND);
        }
        $model->update($request->all());
        return $this->respond(Response::HTTP_OK, $model);
    }

    /**
     * Delete record by ID
     * 
     * @param Request $request
     * @return void
     */
    public function remove(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|exists:messages'
        ]);
        $m = self::MODEL;
        if(is_null($m::find($request->id))){
            return $this->respond(Response::HTTP_NOT_FOUND);
        }
        $m::destroy($request->id);
        return $this->respond(Response::HTTP_NO_CONTENT);
    }

    /**
     * Set general response to JSON
     * 
     * @param HTTP-Status $status
     * @param array $data
     * @return JSON response
     */
    protected function respond($status, $data = [])
    {
        return response()->json($data, $status);
    }

}
