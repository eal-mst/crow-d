<?php namespace App\Http\Controllers;

use Illuminate\Http\Response;

class UserController extends Controller {

    const MODEL = "App\User";

    use RESTActions;

    /**
     * Show one record by ID
     * 
     * @param int $id
     * @return JSON
     */
    public function get($id)
    {
        $m = self::MODEL;
        $model = $m::whereId($id)->with('role')->get();
        if(is_null($model)){
            return $this->respond(Response::HTTP_NOT_FOUND);
        }
        return $this->respond(Response::HTTP_OK, $model);
    }

    /**
     * Return junction
     * 
     * @param int $id
     * @return JSON
     */
    public function getstores($id)
    {
        $m = self::MODEL;
        $model = $m::find($id);
        $result = $model->stores;
        if(is_null($model)){
            return $this->respond(Response::HTTP_NOT_FOUND);
        }
        return $this->respond(Response::HTTP_OK, $model);
    }

    /**
     * Return role
     * 
     * @param int $id
     * @return JSON
     */
    public function getrole($id)
    {
        $m = self::MODEL;
        $model = $m::find($id);
        $result = $model->role;
        if(is_null($result)){
            return $this->respond(Response::HTTP_NOT_FOUND);
        }
        return $this->respond(Response::HTTP_OK, $result);
    }
}
