<!DOCTYPE html>
<html lang="en-US">
    <head>
    <title>Crow-D</title>
    
    <!-- Load Bootstrap CSS -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
    <link href="css/cover.css" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    </head>
    <body>
        <p></p>
        <div class="site-wrapper">

      <div class="site-wrapper-inner">

        <div class="cover-container">
          <main role="main" class="inner cover">
            <p class="lead">
              <img src="img/logo.png" width="20%" />
            </p>
            <h1 class="cover-heading">This is an API! :D </h1>
            <p class="lead">Go away little birds, shoo shoo...</p>
            <p class="lead">
              <img src="img/scarecrow.svg" width="30%" />
            </p>
          </main>
        </div>

      </div>

    </div>

    </body>
</html>