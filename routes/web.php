<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', ['as' => 'home', function () use ($router) {
    return view('index');
}]);

/**
 * Routes for resource Store API
 */
$router->group(['prefix' => 'api'], function() use ($router)
{
    //Matches "/api/" URL
    $router->get('store/read', 'StoreController@all');
    $router->get('store/', 'StoreController@all');
    $router->get('store/{id}', 'UserController@get');
    $router->get('store/read/{id}', 'StoreController@get');
    $router->get('store/read/{id}/products', 'StoreController@getproducts');
    $router->get('store/read/{id}/users', 'StoreController@getusers');
    $router->post('store/create', 'StoreController@add');
    $router->post('store/edit/{id}', 'StoreController@put');
    $router->post('store/delete', 'StoreController@remove');

    //Matches "/api/" URL
    $router->get('user/read', 'UserController@all');
    $router->get('user/', 'UserController@all');
    $router->get('user/{id}', 'UserController@get');
    $router->get('user/read/{id}', 'UserController@get');
    $router->get('user/read/{id}/stores', 'UserController@getstores');
    $router->get('user/read/{id}/role', 'UserController@getusers');
    $router->post('user/create', 'UserController@add');
    $router->post('user/edit/{id}', 'UserController@put');
    $router->post('user/delete', 'UserController@remove');

    //Matches "/api/" URL
    $router->get('order/read', 'OrderController@all');
    $router->get('order/', 'OrderController@all');
    $router->get('order/{id}', 'OrderController@get');
    $router->get('order/read/{id}', 'OrderController@get');
    $router->get('order/read/{id}/products', 'OrderController@getproducts');
    $router->post('order/create', 'OrderController@add');
    $router->post('order/edit/{id}', 'OrderController@put');
    $router->post('order/delete', 'OrderController@remove');
});