<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Store extends Model {

    protected $fillable = [];

    protected $dates = [];

    public static $rules = [
        // Validation rules
    ];

    // Relationships
    public function products()
    {
        return $this->belongsToMany('App\Product', 'products_for_stores');
    }
    
    public function users()
    {
        return $this->belongsToMany('App\User', 'store_users');
    }
}
