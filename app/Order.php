<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model {

    protected $fillable = [];

    protected $dates = [];

    public static $rules = [
        // Validation rules
    ];

    // Relationships
    public function products()
    {
        return $this->belongsToMany('App\Product', 'order_products');
    }

    public function store()
    {
        return $this->hasOne('App\Store', 'id', 'store_id');
    }

}
