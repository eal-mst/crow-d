<?php namespace App\Http\Controllers;

use Illuminate\Http\Response;

class StoreController extends Controller {

    const MODEL = "App\Store";

    use RESTActions;

    /**
     * Show one record by ID
     * 
     * @param int $id
     * @return JSON
     */
    public function get($id)
    {
        $m = self::MODEL;
        $model = $m::whereId($id)->with('products')->get();
        if(is_null($model)){
            return $this->respond(Response::HTTP_NOT_FOUND);
        }
        return $this->respond(Response::HTTP_OK, $model);
    }

     /**
     * Return products
     * 
     * @param int $id
     * @return JSON
     */
    public function getproducts($id)
    {
        $m = self::MODEL;
        $model = $m::find($id);
        $result = $model->products;
        if(is_null($result)){
            return $this->respond(Response::HTTP_NOT_FOUND);
        }
        return $this->respond(Response::HTTP_OK, $result);
    }

    /**
     * Return users
     * 
     * @param int $id
     * @return JSON
     */
    public function getusers($id)
    {
        $m = self::MODEL;
        $model = $m::find($id);
        $result = $model->users;
        if(is_null($result)){
            return $this->respond(Response::HTTP_NOT_FOUND);
        }
        return $this->respond(Response::HTTP_OK, $result);
    }
}
